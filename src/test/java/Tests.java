import Utils.Arquivo;
import Utils.VerificadorVariavel;
import io.restassured.response.ResponseBody;
import org.junit.Assert;
import org.junit.Test;

import java.io.FileNotFoundException;
import java.util.List;

public class Tests
{
    servicesAPI servicesApi = new servicesAPI();
    List<String[]> conteudoArquivo;

    @Test
    public void CT001() throws FileNotFoundException //testar CPF com restrição, valida retornos e mensagem de retorno
    {
        conteudoArquivo = Arquivo.LerArquivo("cpf_restritivos.csv");
        for (int i = 0; i < conteudoArquivo.size(); i++)
        {
            System.out.println("Verificando linha " + (i+1) + "...");
            int returnStatus = servicesApi.consultarRestricaoCpf(conteudoArquivo.get(i)[0]).getStatusCode();
            if (returnStatus == 200)
            {
                ResponseBody corpoRetornadoServico = servicesApi.consultarRestricaoCpf(conteudoArquivo.get(i)[0]).getBody();
                String bodyStringValue = corpoRetornadoServico.asString();
                Assert.assertTrue(bodyStringValue.contains("O CPF " + conteudoArquivo.get(i)[0] + " possui restrição"));
            }
            else if (returnStatus == 204)
            {
                System.out.println("Problema com cpf restritivo de numero: " + conteudoArquivo.get(i)[0]);
                Assert.assertTrue(returnStatus == 200);
            }
            else if (returnStatus != 200 || returnStatus != 204)
            {
                System.out.println("Status de retorno inesperado.");
                Assert.assertTrue(returnStatus == 200);
            }
            System.out.println("Linha " + (i+1) + " verificada com sucesso");
        }
    }

    @Test
    public void CT002() throws FileNotFoundException //testar a criação de uma simulação, valida seus campos, seus retornos e mensagem de retornos
    {
        conteudoArquivo = Arquivo.LerArquivo("criar_simulacao.csv");
        for (int i = 0; i < conteudoArquivo.size(); i++)
        {
            System.out.println("Verificando linha " + (i+1) + "...");
            String[] separarParametros = conteudoArquivo.get(i)[0].split(";", 6);

            String cpf = separarParametros[0];
            String nome = separarParametros[1];
            String email = separarParametros[2];
            String valor = separarParametros[3];
            String parcela = separarParametros[4];
            String seguro = separarParametros[5];

            if (VerificadorVariavel.verificaStringVazia(cpf) && VerificadorVariavel.verificaStringVazia(nome) && VerificadorVariavel.verificaStringVazia(email) && VerificadorVariavel.verificaStringVazia(valor) && VerificadorVariavel.verificaStringVazia(parcela) && VerificadorVariavel.verificaStringVazia(seguro))
            {
                System.out.println("Problema com campo nao preenchido. Verificar dados em: " + conteudoArquivo.get(i)[0]);
            }

            int returnStatus = servicesApi.criarSimulacao(cpf, nome, email, valor, parcela, seguro).getStatusCode();

            if (returnStatus == 201)
            {
                Boolean parametrosCorretos = true;
                if (cpf.length() != 11)
                {
                    System.out.println("CPF invalido encontrado na linha " + (i+1));
                    parametrosCorretos = false;
                }

                if (nome.matches("[a-zA-Z]+"))
                {
                    System.out.println("Nome invalido encontrado na linha " + (i+1));
                    parametrosCorretos = false;
                }

                //TODO - outras validacoes

                ResponseBody corpoRetornadoServico = servicesApi.criarSimulacao(cpf, nome, email, valor, parcela, seguro).getBody();
                String bodyStringValue = corpoRetornadoServico.asString();

                Assert.assertTrue(parametrosCorretos == true && bodyStringValue.contains(separarParametros[0-5]));
            }
            else if (returnStatus == 400)
            {
                System.out.println("Problema detectado com os parametros: " + conteudoArquivo.get(i)[0]);
                Assert.assertTrue(returnStatus == 400);
            }
            else if (returnStatus == 409)
            {
                ResponseBody corpoRetornadoServico = servicesApi.criarSimulacao(cpf, nome, email, valor, parcela, seguro).getBody();
                String bodyStringValue = corpoRetornadoServico.asString();

                Assert.assertTrue(bodyStringValue.contains("CPF já existente"));
            }
            else if (returnStatus != 201 || returnStatus != 400 || returnStatus != 409)
            {
                System.out.println("Status de retorno inesperado.");
                Assert.assertTrue(returnStatus == 201);
            }
        }
    }

    @Test
    public void CT003() //testar se o serviço de put request esta validando todos os campos como obrigatório
    {
        servicesApi.putRequest("base", "basePath", "parameter", "cpf", "nome", "email", "valor", "parcela", "seguro");
    }

    @Test
    public void CT004() //testar retornos de todas simulacoes criadas
    {
        int returnStatus = servicesApi.consultarSimulacoes().getStatusCode();
        System.out.println(returnStatus);

        if (returnStatus == 200)
        {
            ResponseBody corpoRetornadoServico = servicesApi.consultarSimulacoes().getBody();
            String bodyStringValue = corpoRetornadoServico.asString();
            //Assert.assertTrue(!bodyStringValue.isEmpty());
        }
        else if (returnStatus == 204)
        {

        }
        else if (returnStatus != 200 || returnStatus != 204)
        {

        }
    }

    @Test
    public void CT005() //testar retornos de simulacao consultando por CPF
    {

    }

    @Test
    public void CT006() //testar retornos de remocao de simulacao por ID
    {

    }

    @Test
    public void CT007() //testar se o servico de post esta validando todos os campos como obrigatorio
    {

    }
}
