import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.json.simple.JSONObject;


public class servicesAPI
{
    public Response getRequest(String base, String basePath)
    {
        RestAssured.baseURI = base;
        RestAssured.basePath = basePath;
        RequestSpecification request = RestAssured.given();
        Response response = request.accept(ContentType.JSON).get();
        return response;
    }

    public Response getRequest(String base, String basePath, String parameter)
    {
        RestAssured.baseURI = base;
        RestAssured.basePath = basePath;
        RequestSpecification request = RestAssured.given();
        Response response = request.accept(ContentType.JSON).get(parameter);
        return response;
    }

    public Response postRequest(String base, String basePath, String cpf, String nome, String email, String valor, String parcela, String seguro)
    {
        RestAssured.baseURI = base;
        RestAssured.basePath = basePath;
        RequestSpecification request = RestAssured.given();
        JSONObject requestParams = new JSONObject();
        requestParams.put("cpf", cpf);
        requestParams.put("nome", nome);
        requestParams.put("email", email);
        requestParams.put("valor", valor);
        requestParams.put("parcela", parcela);
        requestParams.put("seguro", seguro);
        Response response = request.accept(ContentType.JSON).get();
        return response;
    }

    public Response putRequest(String base, String basePath, String parameter, String cpf, String nome, String email, String valor, String parcela, String seguro)
    {
        RestAssured.baseURI = base;
        RestAssured.basePath = basePath;
        RequestSpecification request = RestAssured.given();

        JSONObject requestParams = new JSONObject();
        requestParams.put("cpf", cpf);
        requestParams.put("nome", nome);
        requestParams.put("email", email);
        requestParams.put("valor", valor);
        requestParams.put("parcela", parcela);
        requestParams.put("seguro", seguro);

        Response response = request.accept(ContentType.JSON).get(parameter);
        return response;
    }

    public Response deleteRequest(String base, String basePath, String parameter)
    {
        RestAssured.baseURI = base;
        RestAssured.basePath = basePath;
        RequestSpecification request = RestAssured.given();
        Response response = request.accept(ContentType.JSON).get(parameter);
        return response;
    }

    public Response consultarRestricaoCpf(String cpf)
    {
        return getRequest("http://localhost:8080", "/api/v1/restricoes/", cpf);
    }

    public Response consultarSimulacoes()
    {
        return getRequest("http://localhost:8080", "/api/v1/simulacoes/");
    }

    public Response consultarSimulacaoCpf(String cpf)
    {
        return getRequest("http://localhost:8080", "/api/v1/simulacoes/", cpf);
    }

    public Response criarSimulacao(String cpf, String nome, String email, String valor, String parcela, String seguro)
    {
        return postRequest("http://localhost:8080", "/api/v1/simulacoes/", cpf,  nome, email, valor, parcela, seguro);
    }

    public Response alterarSimulacao(int cpf)
    {
        return putRequest("http://localhost:8080", "/api/v1/simulacoes/", "",  "", "", "", "", "", "");
    }

    public Response deletarSimulacao(int id)
    {
        return deleteRequest("http://localhost:8080", "/api/v1/simulacoes/", Integer.toString(id));
    }
}
